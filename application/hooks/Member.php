<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member
{
    function login()
    {
        $CI = get_instance();
        $CI->load->library('session');

        if($CI->input->server('PHP_SELF') != "/index.php"){
            if(!$CI->session->has_userdata('bridge_member')){
                location_href('/');
            }else{
                $user = unserialize($CI->session->userdata('bridge_member'));

                $CI->session->set_userdata('bridge_member', serialize($user));
            }
        }
    }
}