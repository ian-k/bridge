<?php
class Room extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if(!$this->session->has_userdata('bridge_member')){
            location_href('/');
        }

        $this->user = unserialize($this->session->userdata('bridge_member'));

        $this->load->model('places/info_model');
        $this->load->model('places/room_model');
        $this->index = $this->get('index');

        if(!$this->index){
            show_404();
            return;
        }

        $this->placeInfo = $this->info_model->getPlacesInfo($this->index, $this->user['index']);

        if(!isset($this->placeInfo['place_site_index'])){
            show_404();
            return;
        }
    }

    function all_get(){
        //객실 타입 목록
        $type = $this->room_model->getRoomTypeLists($this->index);

        $data = array(
            'place' => $this->placeInfo,
            'type' => $type
        );

        $this->load->view('base/header');
        $this->load->view('places/room', $data);
        $this->load->view('base/footer');
    }

    function type_get(){
        $roomTypeIndex = $this->get('roomType');

        if($roomTypeIndex > 0){
            $info = $this->room_model->getRoomTypeInfo($roomTypeIndex);
        }else{
            $info = array(
                'item_type_index' => 0,
                'item_type_name' => ''
            );
        }

        $data = array(
            'info' => $info
        );

        $this->load->view('places/roomTypeInfo', $data);
    }

    function type_post(){
        $data = $this->post();
        $index = $this->get('roomType');

        $data['index'] = $index;

        if(!isset($data['name'])){
            $this->response(array('message' => 'error'), 403);
            return;
        }

        $this->room_model->setRoomType($this->index, $data);

        $this->response(array(), 204);
    }

    function type_patch(){
        $data = $this->patch();
        $index = $this->get('roomType');

        if(!($index > 0)){
            $this->response(array('message' => 'error'), 403);
        }

        $data['index'] = $index;

        if(!isset($data['name'])){
            $this->response(array('message' => 'error'), 403);
            return;
        }

        $this->room_model->setRoomType($this->index, $data);

        $this->response(array(), 204);
    }

    function type_delete(){
        $index = $this->get('roomType');

        $this->room_model->removeRoomType($this->index, $index);

        $this->response(array(), 204);
    }

    function room_get(){
        //객실 목록
        $roomTypeIndex = $this->get('roomType');
        $room = $this->room_model->getRoomLists($this->index, $roomTypeIndex);

        $data = array(
            'place' => $this->placeInfo,
            'roomTypeIndex' => $roomTypeIndex,
            'room' => $room
        );

        $this->load->view('places/roomLists', $data);
    }

    function info_get(){
        $roomTypeIndex = $this->get('roomType');
        $roomIndex = $this->get('roomIndex');

        if($roomIndex > 0){
            $info = $this->room_model->getRoomInfo($this->index, $roomTypeIndex, $roomIndex);
        }else{
            $info = array(
                'place_site_index' => $this->index,
                'item_type_index' => $roomTypeIndex,
                'item_info_index' => $roomIndex,
                'item_info_name' => ''
            );
        }

        $data = array(
            'info' => $info
        );

        $this->load->view('places/roomInfo', $data);
    }

    function info_post(){
        $index = $this->get('roomIndex');
        $data = $this->post();

        $data['index'] = $index;

        if(!isset($data['name'])){
            $this->response(array('message' => 'error'), 403);
            return;
        }

        $this->room_model->setRoom($this->index, $data);

        $this->response(array(), 204);
    }

    function info_patch(){
        $index = $this->get('roomIndex');
        $data = $this->patch();

        $data['index'] = $index;

        if(!($index > 0)){
            $this->response(array('message' => 'error'), 403);
        }

        if(!isset($data['name'])){
            $this->response(array('message' => 'error'), 403);
            return;
        }

        $this->room_model->setRoom($this->index, $data);

        $this->response(array(), 204);
    }

    function info_delete(){
        $index = $this->get('roomIndex');

        $this->room_model->removeRoom($this->index, $index);

        $this->response(array(), 204);
    }
}