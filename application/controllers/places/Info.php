<?php
class Info extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if(!$this->session->has_userdata('bridge_member')){
            location_href('/');
        }

        $this->user = unserialize($this->session->userdata('bridge_member'));

        $this->load->model('places/info_model');
    }

    function lists_get(){
        $placesIndex = array();
        for($i=0; $i< count($this->user['places']); $i++){
            array_push($placesIndex, $this->user['places'][$i]['index']);
        }

        $lists = array();

        if(count($placesIndex) > 0){
            $lists = $this->info_model->getPlacesLists($placesIndex, $this->user['index']);
        }

        $data = array('lists' => $lists);

        $this->load->view('base/header');
        $this->load->view('places/lists', $data);
        $this->load->view('base/footer');
    }

    function data_get(){
        $index = $this->get('index');

        if($index > 0){
            $info = $this->info_model->getPlacesInfo($index, $this->user['index']);
        }else{
            $info = array(
                'place_site_index' => 0,
                'place_site_name' => '',
                'place_site_zip' => '',
                'place_site_address' => '',
                'place_site_addressDetail' => '',
                'place_site_latitude' => '33.450701',
                'place_site_longitude' => '126.570667'
            );
        }

        $data = array('info' => $info);

        $this->load->view('places/info', $data);
    }

    function data_post(){
        $data = $this->post();
        $data = $this->requiredData($data);

        $user = unserialize($this->session->userdata('bridge_member'));

        $index = $this->info_model->setPlacesInfo($data);
        $this->info_model->setPlaceMembrer($user['index'], $index);

        $placeInfo = array(
            'index' => $index,
            'name' => $data['name']
        );

        array_push($user['places'], $placeInfo);

        $this->session->set_userdata('bridge_member', serialize($user));

        $this->response(array(), 204);
    }

    function data_patch(){
        $data = $this->patch();
        $data = $this->requiredData($data);

        $this->info_model->setPlacesInfo($data);

        $this->response(array(), 204);
    }

    function data_delete(){
        $index = $this->delete('index');
        $user = unserialize($this->session->userdata('bridge_member'));

        $this->info_model->removePlacesInfo($index);
        $this->info_model->removePlaceMembrer($user['index'], $index);
    }

    function requiredData($data){
        $required = array('index', 'name','zip','address','latitude','longitude');
        $requiredStatus = true;
        $requiredValue = true;

        for($i=0; $i< count($required); $i++){
            if(!isset($data[$required[$i]])){
                $requiredStatus = false;
            }else{
                if(trim($data[$required[$i]]) == ""){
                    $requiredValue = false;
                }
            }
        }

        if(!$requiredStatus || !$requiredValue){
            $this->response(array('message' => 'Empty Required'), 403);
            exit;
        }

        return $data;
    }
}