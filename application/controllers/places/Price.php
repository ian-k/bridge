<?php
class Price extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if(!$this->session->has_userdata('bridge_member')){
            location_href('/');
        }

        $this->user = unserialize($this->session->userdata('bridge_member'));

        $this->load->model('places/info_model');
        $this->load->model('places/room_model');
        $this->load->model('places/price_model');

        $this->index = $this->get('index');

        if(!$this->index){
            show_404();
            return;
        }

        $this->placeInfo = $this->info_model->getPlacesInfo($this->index, $this->user['index']);

        if(!isset($this->placeInfo['place_site_index'])){
            show_404();
            return;
        }
    }

    function all_get(){
        $priceType = $this->get('priceType');
        if(!$priceType){
            $priceType = "T";
        }

        $type = $this->room_model->getRoomTypeLists($this->index);
        $priceLists = $this->price_model->getPriceLists($this->index, $priceType);

        $price = array();
        for($i=0; $i< count($priceLists); $i++){
            if(!isset($price[$priceLists[$i]['item_type_index']])){
                $price[$priceLists[$i]['item_type_index']] = array();
            }
            array_push($price[$priceLists[$i]['item_type_index']], $priceLists[$i]);
        }

        $data = array(
            'place' => $this->placeInfo,
            'type' => $type,
            'price' => $price,
            'priceType' => $priceType
        );

        $this->load->view('base/header');
        $this->load->view('places/price', $data);
        $this->load->view('base/footer');
    }

    function info_get(){
        $typeIndex = $this->get('typeIndex');
        $priceIndex = $this->get('priceIndex');
        $priceType = $this->get('priceType');
        if(!$priceType){
            $priceType = "T";
        }

        if((int)$priceIndex > 0){
            $info = $this->price_model->getPriceInfo($this->index, $priceIndex);
        }else{
            $info = array(
                'item_price_index' => $priceIndex,
                'place_site_index' => $this->index,
                'item_type_index' => $typeIndex,
                'item_price_type' => $priceType,
                'item_price_start' => date('Y-m-d'),
                'item_price_end' => date('Y-m-d', strtotime('+1 year')),
                'item_price_mon' => 0,
                'item_price_tue' => 0,
                'item_price_wen' => 0,
                'item_price_thu' => 0,
                'item_price_fri' => 0,
                'item_price_sat' => 0,
                'item_price_sun' => 0,
                'item_price_add' => 0
            );
        }

        $data = array(
            'info' => $info
        );

        $this->load->view('places/priceInfo', $data);
    }

    function info_post(){
        $data = $this->post();
        $data = $this->requiredData($data);

        $data['index'] = (int)$data['itemPriceIndex'];

        $this->price_model->setPriceInfo($this->index, $data);

        $this->response(array(), 204);
    }

    function info_patch(){
        $data = $this->patch();
        $data = $this->requiredData($data);

        $data['index'] = (int)$data['itemPriceIndex'];

        $this->price_model->setPriceInfo($this->index, $data);

        $this->response(array(), 204);
    }

    function info_delete(){
        $index = $this->delete('index');
        $user = unserialize($this->session->userdata('bridge_member'));

        $this->places_model->removePlacesInfo($index);
        $this->places_model->removePlaceMembrer($user['index'], $index);
    }

    function requiredData($data){
        $required = array('type','start','end','mon','tue','wen','thu','fri','sat','sun');
        $requiredStatus = true;
        $requiredValue = true;

        for($i=0; $i< count($required); $i++){
            if(!isset($data[$required[$i]])){
                $requiredStatus = false;
            }else{
                if(trim($data[$required[$i]]) == ""){
                    $requiredValue = false;
                }
            }
        }

        if(!$requiredStatus || !$requiredValue){
            $this->response(array('message' => 'Empty Required'), 403);
            exit;
        }

        return $data;
    }
}