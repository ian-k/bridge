<?php
class Calendar extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->session->has_userdata('bridge_member')) {
            location_href('/');
        }

        $this->load->model('booking/booking_model');
        $this->load->model('places/room_model');
        $this->load->model('places/info_model');
        $this->user = unserialize($this->session->userdata('bridge_member'));

        $this->index = $this->user['places'][0]['index'];
        $this->name = $this->user['places'][0]['name'];
    }

    function data_get()
    {
        $setDate = $this->get('setDate');
        if(!$setDate){
            $setDate = date('Y-m-d');
        }
        $setDate = date('Y-m-d', strtotime($setDate));

        $dateCount = 3;
        if($dateCount > 5){
            $dateCount = 5;
        }
        $date = array();
        for($i=0; $i< $dateCount; $i++){
            array_push($date, date('Y-m-d', strtotime('+ '.$i.'day', strtotime($setDate))));
        }

        $info = $this->info_model->getPlacesInfo($this->index, $this->user['index']);
        $item = $this->room_model->getPlaceRoomLists($this->index);
        $bookingLists = $this->booking_model->getBookingDate($this->index, $date);

        $booking = array();
        for($i=0; $i< count($bookingLists); $i++){
            $bookingDate = date('Y-m-d', strtotime($bookingLists[$i]['booking_info_checkin']));

            if(!isset($booking[$bookingLists[$i]['item_info_index']][$bookingDate])){
                $booking[$bookingLists[$i]['item_info_index']][$bookingDate] = array();
            }
            array_push($booking[$bookingLists[$i]['item_info_index']][$bookingDate], $bookingLists[$i]);
        }

        $data = array(
            'info' => $info,
            'item' => $item,
            'date' => $date,
            'booking' => $booking,
            'setDate' => $setDate,
            'prevDate' => date('Y-m-d', strtotime('-1 day', strtotime($setDate))),
            'nextDate' => date('Y-m-d', strtotime('+1 day', strtotime($setDate)))
        );

        $this->load->view('base/header', array('name' => $this->name));
        $this->load->view('calendar', $data);
        $this->load->view('base/footer');
    }
}