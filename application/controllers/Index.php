<?php
class Index extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index(){
        if($this->session->has_userdata('bridge_member')){
            location_href('/calendar');
        }else{
            $this->load->view('login');
        }
    }
}