<?php
class Member extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('privacy/member_model');
    }

    function login_post(){
        $id = $this->input->post('id');
        $pw = $this->input->post('pw');

        if(!$id | !$pw){
            alert('잘못된 접근입니다');
            history_back();
            return;
        }
        $user = $this->member_model->getMemberInfo($id);

        if(!isset($user['member_info_index'])){
            history_back('아이디 또는 비밀번호가 다릅니다');
            return;
        }

        if(encrypt_check($pw, $user['member_info_pw'])){
            //로그인 처리
            $this->setUserSession($user['member_info_index'], $user['member_info_auth']);
        }else{
            alert('아이디 또는 비밀번호가 다릅니다');
            history_back();
            return;
        }
    }

    function logout_get(){
        if($this->session->has_userdata('bridge_member')){
            $this->unsetUserSession();
        }
        location_href('/');
    }

    function sign_post(){
        $id = $this->input->post('id');
        $pw = $this->input->post('pw');
        $encript = $this->input->post('encrypt');

        if(!$id | !$pw | !$encript){
            alert('잘못된 접근입니다');
            history_back();
            return;
        }

        $encryptInfo = $this->member_model->getEncryptInfo($encript);

        if(isset($encryptInfo['place_site_index'])){
            $index = $this->member_model->signMemberInfo($id, encrypt($pw), $encryptInfo['place_site_index']);

            $this->setUserSession($index);
        }
    }

    function setUserSession($index, $auth = 0)
    {
        $placeLists = $this->member_model->getPlaceLists($index);

        $places = array();
        for ($i = 0; $i < count($placeLists); $i++) {
            $places[$i]['index'] = $placeLists[$i]['place_site_index'];
            $places[$i]['name'] = $placeLists[$i]['place_site_name'];
        }

        $user = array(
            'index' => $index,
            'auth' => $auth,
            'places' => $places
        );

        $this->session->set_userdata('bridge_member', serialize($user));

        $this->member_model->setLastLogin($index);

        location_href('/calendar');
    }

    function unsetUserSession(){
        $this->session->unset_userdata('bridge_member');
    }

    function adminSign_get(){
        $id = $this->input->get('id');
        $pw = $this->input->get('pw');

        $index = $this->member_model->setMemberInfo($id, encrypt($pw));

        $this->setUserSession($index);
    }
}