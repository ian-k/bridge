<?php
class Info extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if(!$this->session->has_userdata('bridge_member')){
            location_href('/');
        }

        $this->load->model('booking/booking_model');
        $this->load->model('places/room_model');
        $this->user = $this->session->userdata('bridge_member');
        $this->index = $this->get('index');
    }

    function room_get(){
        $typeIndex = $this->get('typeIndex');
        $type = $this->get('type');
        $checkin = date('Y-m-d H:i:s', strtotime(urldecode($this->get('checkin')).":00"));
        $checkout = date('Y-m-d H:i:s', strtotime(urldecode($this->get('checkout')).":00"));

        if(!$type){
            $type = "T";
        }

        $day = round(abs(strtotime(date('Y-m-d', strtotime($checkout)))-strtotime(date('Y-m-d', strtotime($checkin))))/86400);
        if($day < 1){
            $day = 1;
        }
        $price = 0;
        $priceArray = array(
            '0' => 'sun',
            '1' => 'mon',
            '2' => 'tue',
            '3' => 'wen',
            '4' => 'thu',
            '5' => 'fri',
            '6' => 'sat'
        );

        for($i=0; $i< $day; $i++){
            $setDate = date('Y-m-d', strtotime('+'.$i.' day', strtotime($checkin)));
            $priceInfo = $this->booking_model->getTypePrice($typeIndex, $type, $setDate);

            if(isset($priceInfo['item_price_index'])){
                $price += $priceInfo['item_price_'.$priceArray[date('w', strtotime($setDate))]];
            }
        }

        $lists = $this->booking_model->getRoomLists($this->index, $typeIndex, $checkin);

        $result = array();
        for($i=0; $i< count($lists); $i++){
            $result[$i] = array(
                'index' => (int)$lists[$i]['item_info_index'],
                'name' => $lists[$i]['item_info_name'],
                'price' => $price
            );
        }

        $this->response($result, 200);
    }

    function data_get(){
        $bookingIndex = $this->get('bookingIndex');

        if((int)$bookingIndex > 0){
            $info = $this->booking_model->getBookingInfo($this->index, $bookingIndex);

            $decryptData = array(
                'key' => $info['booking_info_encrypt'],
                'data' => array(
                    'name' => $info['booking_info_name'],
                    'mobile' => $info['booking_info_mobile']
                )
            );

            $decrypt = bridge_decrypt($decryptData);

            $info['booking_info_name'] = $decrypt['name'];
            $info['booking_info_mobile'] = $decrypt['mobile'];
        }else{
            $info = array(
                'booking_info_index' => 0,
                'place_site_index' => $this->index,
                'item_type_index' => (int)0,
                'item_info_index' => (int)0,
                'booking_route_index' => '',
                'booking_info_type' => 'T',
                'booking_info_checkin' => date('Y-m-d H:i'),
                'booking_info_checkout' => date('Y-m-d H:i', strtotime('+3 hour')),
                'booking_info_name' => '',
                'booking_info_mobile' => '',
                'booking_info_name_hash' => '',
                'booking_info_mobile_hash' => '',
                'booking_info_traffic' => 0,
                'booking_info_request' => '',
                'booking_info_payPrice' => (int)0,
                'booking_info_contractPrice' => (int)0,
                'booking_info_method' => 'card',
                'booking_info_state' => 'success',
            );
        }

        $route = $this->booking_model->getBookingRoute($this->index);
        $itemType = $this->room_model->getRoomTypeLists($this->index);
        $item = array();
        if((int)$info['item_type_index'] > 0){
            $item = $this->room_model->getRoomLists($this->index, $info['item_type_index']);
        }

        $data = array(
            'info' => $info,
            'config' => bridge_booking_config(),
            'route' => $route,
            'itemType' => $itemType,
            'item' => $item
        );

        $this->load->view('booking/info', $data);
    }

    function data_post(){
        $data = $this->post();

        $this->requiredData($data);

        $this->booking_model->setBookingInfo($this->index, $data);

        $this->response(array(), 204);
    }

    function data_patch(){
        $data = $this->patch();

        $this->requiredData($data);

        $this->booking_model->setBookingInfo($this->index, $data);

        $this->response(array(), 204);
    }

    function data_delete(){
        $bookingIndex = $this->get('bookingIndex');
    }

    function requiredData($data){
        $required = array('route','typeIndex','itemIndex','type','checkin','checkout','traffic','payPrice','contractPrice','method','state');
        $requiredStatus = true;
        $requiredValue = true;
        $requiredFiled = "";

        for($i=0; $i< count($required); $i++){
            if(!isset($data[$required[$i]])){
                $requiredStatus = false;
                $requiredFiled .= ",".$required[$i];
            }else{
                if(trim($data[$required[$i]]) == ""){
                    $requiredValue = false;
                    $requiredFiled .= ",".$required[$i];
                }
            }
        }

        if(!$requiredStatus || !$requiredValue){
            $this->response(array('message' => 'Empty Required'), 403);
            exit;
        }

        return $data;
    }
}