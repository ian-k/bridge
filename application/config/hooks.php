<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$hook['post_controller_constructor'] = array(
    'class'    => 'Member',
    'function' => 'login',
    'filename' => 'Member.php',
    'filepath' => 'hooks'
);
