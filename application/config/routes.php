<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*
| -------------------------------------------------------------------------
| REST API Routes
| -------------------------------------------------------------------------
*/

/* booking page */
$route['calendar']                          = 'calendar/data';

/* member page */
$route['bridge/login']                      = 'member/login';
$route['bridge/logout']                     = 'member/logout';
$route['bridge/sign']                       = 'member/sign';

/* info page */
$route['places']                            = 'places/info/lists';
$route['places/(:any)/info']                = 'places/info/data/index/$1';

$route['places/(:any)/roomtypes']                       = 'places/room/all/index/$1';
$route['places/(:any)/roomtypes/(:any)']                = 'places/room/type/index/$1/roomType/$2';
$route['places/(:any)/roomtypes/(:any)/room']           = 'places/room/room/index/$1/roomType/$2';
$route['places/(:any)/roomtypes/(:any)/room/(:any)']    = 'places/room/info/index/$1/roomType/$2/roomIndex/$3';

$route['places/(:any)/price']                       = 'places/price/all/index/$1';
$route['places/(:any)/price/(:num)/info/(:num)']    = 'places/price/info/index/$1/typeIndex/$2/priceIndex/$3';

$route['booking/(:any)/room/(:any)']    = 'booking/info/room/index/$1/typeIndex/$2';
$route['booking/(:any)/info']           = 'booking/info/data/index/$1';
$route['booking/(:any)/info/(:any)']    = 'booking/info/data/index/$1/bookingIndex/$2';

