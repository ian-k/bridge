<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pension_lib {
	public function __construct() {
		
	}
	
	public function replacePhone($str){
        $number1 = substr($str,0,3);
        $numberSub1 = 3;
        if(substr($str,0,2) == "02"){
            $number1 = substr($str,0,2);
            $numberSub1 = 2;
        }
        $number2 = substr($str,$numberSub1,4);
        $numberSub2 = 4;
        
        $number3 = substr($str,($numberSub1+$numberSub2));
        if(strlen($number3) == 3){
            $number2 = substr($str,$numberSub1,3);
            $numberSub2 = 3;
            $number3 = substr($str,($numberSub1+$numberSub2));
        }
        
        return $number1."-".$number2."-".$number3;
    }

}