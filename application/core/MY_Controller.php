<?php defined('BASEPATH') OR exit('See you next time.');

require APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class MY_Controller extends REST_Controller{
	
    public function __construct(){
        parent::__construct();
        
		$this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
}

