<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function bridge_mask_mobile($value){
    $value = str_replace('-','', $value);

    if(substr($value,0,2) == "02"){
        $re = '/^(\d{2})(\d{4})(\d+|\*)$/';
    }else if(substr($value,0,3) == "050"){
        $re = '/^(\d{4})(\d{4})(\d+|\*)$/';
    }else{
        $re = '/^(\d{3})(\d{4})(\d+|\*)$/';
    }
    $subst = '$1****$3';
    return preg_replace($re, $subst, $value);

}
function bridge_mask_ip($value){
    $re = '/(?!\d{1,3}\.\d{1,3}\.)\d/';
    $subst = "*";

    return preg_replace($re, $subst, $value);
}
function bridge_mask_email($value){
    if($value == ""){
        return $value;
    }
    $valueArray = explode("@", $value);

    if(strlen($valueArray[0]) > 3){
        $masking_email = str_pad(substr($valueArray[0],0,3), strlen($valueArray[0]), "*", STR_PAD_RIGHT);
    }else if(strlen($valueArray[0]) > 2){
        $masking_email = str_pad(substr($valueArray[0],0,2), strlen($valueArray[0]), "*", STR_PAD_RIGHT);
    }else if(strlen($valueArray[0]) > 1){
        $masking_email = str_pad(substr($valueArray[0],0,1), strlen($valueArray[0]), "*", STR_PAD_RIGHT);
    }else{
        $masking_email = "*";
    }

    return $masking_email.'@'.$valueArray[1];
}
function bridge_mask_name($value){
    $return = mb_substr($value,0,1);
    for($i=1; $i< mb_strlen($value); $i++){
        $return .= "*";
    }

    return $return;
}