<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function location_href($uri){
    echo "<script> location.href = '".$uri."'; </script>";
}

function alert($message){
    echo "<script> alert('".$message."'); </script>";
}

function history_back(){
    echo "<script> history.back(-1); </script>";
}