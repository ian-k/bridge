<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function mobilePattern($str){
    $number1 = substr($str,0,3);
    $numberSub1 = 3;
    if(substr($str,0,2) == "02"){
        $number1 = substr($str,0,2);
        $numberSub1 = 2;
    }
    $number2 = substr($str,$numberSub1,4);
    $numberSub2 = 4;

    $number3 = substr($str,($numberSub1+$numberSub2));
    if(strlen($number3) == 3){
        $number2 = substr($str,$numberSub1,3);
        $numberSub2 = 3;
        $number3 = substr($str,($numberSub1+$numberSub2));
    }

    return $number1."-".$number2."-".$number3;
}

function parseInt($value){
    return (int)trim(str_replace(',','',$value));
}

function bridge_booking_config(){
    $result = array(
        'type' => array(
            'T' => '대실',
            'S' => '숙박'
        ),
        'traffic' => array(
            '0' => '도보',
            '1' => '차량'
        ),
        'method' => array(
            'card' => '카드',
            'account' => '계좌입금',
            'cash' => '현금결제'
        ),
        'state' => array(
            'success' => '예약',
            'cancel' => '취소',
            'contract' => '가예약'
        )
    );

    return $result;
}