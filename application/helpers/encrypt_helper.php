<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function encrypt($pw) {
    //단방향 암호화 함수, 비밀번호에서 사용
    $options = [
        'cost' => 12
    ];

    return base64_encode(password_hash($pw, PASSWORD_BCRYPT, $options));
}

function bridge_hash($data){
    return strtoupper(hash('sha512', md5($data)));
}

function encrypt_check($pw, $comparison){
    if(password_verify($pw, base64_decode($comparison))){
        return true;
    }else{
        return false;
    }
}

function bridge_encrypt($data) {
    $result = array();
    $encryptInfo = get_encrypt_key();

    $dataKey = $encryptInfo['key'];
    $encryptKey = $encryptInfo['encrypt'];
    $iv = get_encrypt_iv($encryptInfo['encrypt']);

    foreach($data as $key => $val){
        $data[$key] = bridge_data_encrypt($dataKey, $val, $iv);
    }

    $result['key'] = $encryptKey;
    $result['data'] = $data;

    return $result;
}

function bridge_data_encrypt($key, $data, $iv){
    return str_replace("=", "", base64_encode(openssl_encrypt($data, "AES-256-CBC", $key, 0, $iv)));
}

function bridge_decrypt($data){
    $dataKey = get_decrypt_key($data['key']);
    $iv = get_encrypt_iv($data['key']);
    $data = $data['data'];

    foreach($data as $key => $val){
        $data[$key] = bridge_data_decrypt($dataKey, $val, $iv);
    }

    return $data;
}

function bridge_data_decrypt($key, $data, $iv){
    return openssl_decrypt(base64_decode($data), "AES-256-CBC", $key, 0, $iv);
}



function get_decrypt_key($encryptKey){
    //decrypt key 호출
    return '#+Bridge+Encrypt+Key+#';
    /*
    require_once BASEPATH.'/../application/third_party/AWS-Library/aws-autoloader.php';

    $CI =& get_instance();
    $CI->load->config('aws_config');

    $config = $CI->config->item('kms');

    $KmsClient = Aws\Kms\KmsClient::factory($config);

    $result = $KmsClient->decrypt([
        'CiphertextBlob' => base64_decode($encryptKey)
    ]);

    if($result->get('@metadata')['statusCode'] == "200"){
        return base64_encode($result->get('Plaintext'));
    }else{
        return false;
    }
    */
}

function get_encrypt_iv($data){
    return substr(strtoupper(hash('sha512', md5($data))),0,16);
}

function get_encrypt_key(){
    return array(
        'key' => '#+Bridge+Encrypt+Key+#',
        'encrypt' => base64_encode('#+Bridge+Encrypt+Key+#')
    );
}