<?php
class Room_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', true);
    }

    function getPlaceRoomLists($index){
        $this->db->select('item_info.item_info_index, item_info.item_info_name, item_info.place_site_index');
        $this->db->where('item_type.place_site_index', $index);
        $this->db->order_by('item_type.item_type_sort','ASC');
        $this->db->order_by('item_info.item_info_sort','ASC');
        $this->db->join('item_info AS item_info','item_info.item_type_index = item_type.item_type_index AND item_info.place_site_index = item_type.place_site_index','LEFT');
        $result = $this->db->get('item_type AS item_type')->result_array();

        return $result;
    }

    function getRoomTypeLists($index){
        $this->db->where('place_site_index', $index);
        $result = $this->db->get('item_type')->result_array();

        return $result;
    }

    function getRoomTypeInfo($index){
        $this->db->where('item_type_index', $index);
        $result = $this->db->get('item_type')->row_array();

        return $result;
    }

    function setRoomType($index, $data){
        $set = array(
            'item_type_name' => $data['name'],
        );
        $this->db->set($set);

        if($data['index'] > 0){
            $this->db->where('item_type_index', $data['index']);
            $this->db->where('place_site_index', $index);
            $this->db->set('item_type_uptDate', date('Y-m-d H:i:s'));
            $this->db->update('item_type');
        }else{
            $this->db->set('place_site_index', $index);
            $this->db->set('item_type_regDate', date('Y-m-d H:i:s'));
            $this->db->insert('item_type');
        }
    }

    function getRoomLists($index, $roomTypeIndex){
        $this->db->where('place_site_index', $index);
        $this->db->where('item_type_index', $roomTypeIndex);
        $result = $this->db->get('item_info')->result_array();

        return $result;
    }

    function getRoomInfo($index, $roomTypeIndex, $roomIndex){
        $this->db->where('place_site_index', $index);
        $this->db->where('item_type_index', $roomTypeIndex);
        $this->db->where('item_info_index', $roomIndex);
        $result = $this->db->get('item_info')->row_array();

        return $result;
    }

    function removeRoomType($index, $roomTypeIndex){
        $this->db->where('place_site_index', $index);
        $this->db->where('item_type_index', $roomTypeIndex);
        $this->db->delete('item_type');

        $this->db->where('place_site_index', $index);
        $this->db->where('item_type_index', $roomTypeIndex);
        $this->db->delete('item_info');

        $this->db->where('place_site_index', $index);
        $this->db->where('item_type_index', $roomTypeIndex);
        $this->db->delete('item_price');
    }

    function setRoom($index, $data){
        $set = array(
            'item_info_name' => $data['name'],
            'item_type_index' => $data['itemTypeIndex']
        );
        $this->db->set($set);

        if($data['index'] > 0){
            $this->db->where('item_info_index', $data['index']);
            $this->db->where('place_site_index', $index);
            $this->db->set('item_info_uptDate', date('Y-m-d H:i:s'));
            $this->db->update('item_info');
        }else{
            $this->db->set('place_site_index', $index);
            $this->db->set('item_info_regDate', date('Y-m-d H:i:s'));
            $this->db->insert('item_info');
        }
    }

    function removeRoom($index, $roomIndex){
        $this->db->where('place_site_index', $index);
        $this->db->where('item_info_index', $roomIndex);
        $this->db->delete('item_info');
    }
}