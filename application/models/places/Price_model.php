<?php
class Price_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', true);
    }

    function getPriceLists($index, $priceType = 'T'){
        $this->db->where('place_site_index', $index);
        $this->db->where('item_price_type', $priceType);
        $result = $this->db->get('item_price')->result_array();

        return $result;
    }

    function getPriceInfo($index, $priceIndex){
        $this->db->where('item_price_index', $priceIndex);
        $this->db->where('place_site_index', $index);
        $result = $this->db->get('item_price')->row_array();

        return $result;
    }

    function setPriceInfo($index, $data){
        $this->db->set(array(
            'item_type_index' => $data['itemTypeIndex'],
            'item_price_type' => $data['type'],
            'item_price_start' => date('Y-m-d', strtotime($data['start'])),
            'item_price_end' => date('Y-m-d', strtotime($data['end'])),
            'item_price_mon' => parseInt($data['mon']),
            'item_price_tue' => parseInt($data['tue']),
            'item_price_wen' => parseInt($data['wen']),
            'item_price_thu' => parseInt($data['thu']),
            'item_price_fri' => parseInt($data['fri']),
            'item_price_sat' => parseInt($data['sat']),
            'item_price_sun' => parseInt($data['sun']),
            'item_price_uptDate' => date('Y-m-d H:i:s')
        ));
        if($data['index'] > 0){
            $this->db->where(array(
                'place_site_index' => $index,
                'item_price_index' => $data['index']
            ));
            $this->db->update('item_price');
        }else{
            $this->db->set(array(
                'place_site_index' => $index,
                'item_price_regDate' => date('Y-m-d H:i:s')
            ));
            $this->db->insert('item_price');
        }
    }
}