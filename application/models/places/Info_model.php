<?php
class Info_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', true);
    }

    function getPlacesLists($index, $memberIndex){
        $this->db->select('place_site.*');
        $this->db->where('place_member.place_member_index IS NOT NULL','', false);
        $this->db->where_in('place_site.place_site_index', $index);
        $this->db->where('place_site.place_site_status', true);
        $this->db->group_by('place_site.place_site_index');
        $this->db->join('place_member AS place_member',"place_member.place_site_index = place_site.place_site_index AND place_member.member_info_index = '".$memberIndex."'",'LEFT');
        $result = $this->db->get('place_site AS place_site')->result_array();

        return $result;
    }

    function getPlacesInfo($index, $memberIndex){
        $this->db->select('place_site.*');
        $this->db->where('place_member.place_member_index IS NOT NULL','', false);
        $this->db->where('place_site.place_site_index', $index);
        $this->db->where('place_site.place_site_status', true);
        $this->db->group_by('place_site.place_site_index');
        $this->db->join('place_member AS place_member',"place_member.place_site_index = place_site.place_site_index AND place_member.member_info_index = '".$memberIndex."'",'LEFT');
        $result = $this->db->get('place_site AS place_site')->row_array();

        return $result;
    }

    function setPlacesInfo($data){
        if(!isset($data['addressDetail'])){
            $data['addressDetail'] = "";
        }
        $set = array(
            'place_site_name' => $data['name'],
            'place_site_zip' => $data['zip'],
            'place_site_address' => $data['address'],
            'place_site_addressDetail' => $data['addressDetail'],
            'place_site_latitude' => $data['latitude'],
            'place_site_longitude' => $data['longitude'],
            'place_site_status' => true
        );

        $this->db->set($set);
        if($data['index'] > 0){
            $this->db->where('place_site_index', $data['index']);
            $this->db->update('place_site');

            $place_site_index = $data['index'];
        }else{
            $this->db->set('place_site_regDate', date('Y-m-d H:i:s'));
            $this->db->insert('place_site');

            $place_site_index = $this->db->insert_id();
        }

        return (int)$place_site_index;
    }

    function removePlacesInfo($index){
        $this->db->where('place_site_index', $index);
        $this->db->set('place_site_status',false);
        $this->db->update('place_site');
    }

    function setPlaceMembrer($memberIndex, $placeIndex){
        $this->db->where('member_info_index', $memberIndex);
        $this->db->where('place_site_index', $placeIndex);
        $count = $this->db->count_all_results('place_member');

        if($count == 0){
            $this->db->set('member_info_index', $memberIndex);
            $this->db->set('place_site_index', $placeIndex);
            $this->db->set('place_member_regDate', date('Y-m-d H:i:s'));
            $this->db->insert('place_member');
        }
    }

    function removePlaceMember($memberIndex, $placeIndex){
        $this->db->where('member_info_index', $memberIndex);
        $this->db->where('place_site_index', $placeIndex);
        $this->db->delete('place_member');
    }
}