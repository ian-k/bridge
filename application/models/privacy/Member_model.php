<?php
class Member_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', true);
    }


    function getMemberInfo($id){
        $this->db->select('member_info_index, member_info_id, member_info_pw, member_info_auth');
        $this->db->where('member_info_id', $id);
        $result = $this->db->get('member_info')->row_array();

        return $result;
    }

    function getEncryptInfo($encrypt){
        $this->db->where('place_site_encrypt', $encrypt);
        $this->db->where('place_site_encrypt != ','');
        $result = $this->db->get('place_site')->row_array();

        return $result;
    }

    function signMemberInfo($id, $pw, $place_site_index){
        $member_info_index = $this->setMemberInfo($id, $pw);
        $this->setPlaceMember($member_info_index, $place_site_index);
        $this->clearPlaceEncrypt($place_site_index);

        return $member_info_index;
    }

    function setMemberInfo($id, $pw){
        $this->db->set(array(
            'member_info_id' => $id,
            'member_info_pw' => $pw,
            'member_info_regDate' => date('Y-m-d H:i:s')
        ));
        $this->db->insert('member_info');

        return $this->db->insert_id();
    }

    function setLastLogin($index){
        $this->db->where('member_info_index', $index);
        $this->db->set('member_info_lastLogin', date('Y-m-d H:i:s'));
        $this->db->update('member_info');
    }

    function setPlaceMember($member_info_index, $place_site_index){
        $this->db->set(array(
            'member_info_index' => $member_info_index,
            'place_site_index' => $place_site_index,
            'place_member_regDate' => date('Y-m-d H:i:s')
        ));
        $this->db->insert('place_member');
    }

    function clearPlaceEncrypt($place_site_index){
        $this->db->where('place_site_index', $place_site_index);
        $this->db->set('place_site_encrypt','');
        $this->db->update('place_site');
    }

    function getPlaceLists($index){
        $this->db->select('place_site.place_site_index, place_site.place_site_name');
        $this->db->where('place_member.place_member_index', $index);
        $this->db->join('place_site AS place_site','place_site.place_site_index = place_member.place_site_index','LEFT');
        $result = $this->db->get('place_member AS place_member')->result_array();

        return $result;
    }
}