<?php
class Booking_model extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', true);
    }

    function getRoomLists($index, $typeIndex, $checkin){
        $this->db->where('item_info.place_site_index', $index);
        $this->db->where('item_info.item_type_index', $typeIndex);
        $this->db->where('booking_info.booking_info_index IS NULL','', false);
        $this->db->join('booking_info AS booking_info',"item_info.item_info_index = booking_info.item_info_index AND booking_info.booking_info_checkout >= '".$checkin."'",'LEFT');
        $this->db->select('item_info.item_info_index, item_info.item_info_name, item_info.item_type_index');
        $result = $this->db->get('item_info AS item_info')->result_array();

        return $result;
    }

    function getTypePrice($typeIndex, $type, $setDate){
        $this->db->where('item_type_index', $typeIndex);
        $this->db->where('item_price_type', $type);
        $this->db->where("'".$setDate."' BETWEEN item_price_start AND item_price_end",'', false);
        $result = $this->db->get('item_price')->row_array();

        return $result;
    }

    function getBookingRoute($index){
        $this->db->select('booking_route_index, booking_route_name');
        $this->db->where('place_site_index', $index);
        $result = $this->db->get('booking_route')->result_array();

        return $result;
    }

    function getBookingInfo($index, $bookingIndex){
        $this->db->where('place_site_index', $index);
        $this->db->where('booking_info_index', $bookingIndex);
        $result = $this->db->get('booking_info')->row_array();

        return $result;
    }

    function setBookingInfo($index, $data){
        $name = trim($data['name']);
        $mobile = trim(str_replace('-','',$data['mobile']));

        $encryptData = array(
            'name' => $name,
            'mobile' => $mobile
        );

        $encryptArray = bridge_encrypt($encryptData);
        $encryptKey = $encryptArray['key'];
        $encrypt = $encryptArray['data'];

        $this->db->set(array(
            'place_site_index' => $index,
            'item_type_index' => (int)$data['typeIndex'],
            'item_info_index' => (int)$data['itemIndex'],
            'booking_route_index' => (int)$data['route'],
            'booking_info_type' => $data['type'],
            'booking_info_checkin' => $data['checkin'],
            'booking_info_checkout' => $data['checkout'],
            'booking_info_name' => $encrypt['name'],
            'booking_info_mobile' => $encrypt['mobile'],
            'booking_info_encrypt' => $encryptKey,
            'booking_info_name_hash' => bridge_hash($name),
            'booking_info_mobile_hash' => bridge_hash($mobile),
            'booking_info_name_mask' => bridge_mask_name($name),
            'booking_info_mobile_mask' => bridge_mask_mobile(mobilePattern($mobile)),
            'booking_info_traffic' => $data['traffic'],
            'booking_info_request' => $data['request'],
            'booking_info_payPrice' => (int)str_replace(',','',$data['payPrice']),
            'booking_info_contractPrice' => (int)str_replace(',','',$data['contractPrice']),
            'booking_info_method' => $data['method'],
            'booking_info_state' => $data['state'],
            'booking_info_uptDate' => date('Y-m=d H:i:s')
        ));
        if((int)$data['booking_index'] > 0){
            $this->db->where('booking_info_index', $data['booking_index']);
            $this->db->update('booking_info');
        }else{
            $this->db->set('booking_info_regDate', date('Y-m-d H:i:s'));
            $this->db->insert('booking_info');
        }
    }

    function getBookingDate($index, $date){
        $select = array(
            'booking_info_index',
            'item_type_index',
            'item_info_index',
            'booking_info_name_mask',
            'booking_info_mobile_mask',
            'booking_info_checkin',
            'booking_info_checkout',
            'booking_info_state'
        );
        $this->db->select($select);
        $this->db->where('place_site_index', $index);
        $this->db->where('booking_info_status', 1);
        $this->db->where("booking_info_checkin BETWEEN '".min($date)." 00:00:00' AND '".max($date)." 23:59:59'",'', false);
        $result = $this->db->get('booking_info')->result_array();

        return $result;
    }
}